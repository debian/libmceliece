In the following speed table, smaller numbers are better.
The numbers are interquartile means of single-core cycle counts on various microarchitectures.
Overclocking is disabled.


| μarch |  KEM | keypair | enc  | dec  |
| :---- | :--- | ------: | ---: | ---: |
| Zen 3 (2020) | 348864f | 24106405 | 20804 | 106097
|  | 348864 | 45106138 | 20546 | 106172
|  | 460896f | 71472398 | 50594 | 204050
|  | 460896 | 124642452 | 50096 | 203969
|  | 6688128f | 148441504 | 82287 | 243536
|  | 6688128 | 303266579 | 80930 | 243793
|  | 6960119f | 133277867 | 85401 | 226726
|  | 6960119 | 267002498 | 85845 | 226876
|  | 8192128f | 171644127 | 97821 | 243330
|  | 8192128 | 218895050 | 95026 | 243326
| Cortex-A72 (2016) | 348864f | 199765262 | 184295 | 672946
|  | 348864 | 239409924 | 185373 | 672526
|  | 460896f | 707851621 | 378691 | 1828393
|  | 460896 | 1055319290 | 367034 | 1828230
|  | 6688128f | 4085611413 | 736156 | 2053586
|  | 6688128 | 2816930495 | 733524 | 2053047
|  | 6960119f | 3582775631 | 738712 | 1968519
|  | 6960119 | 1978396919 | 735774 | 1969441
|  | 8192128f | 5243775013 | 815911 | 2016243
|  | 8192128 | 4026465806 | 803289 | 2015899
| Skylake (2015) | 348864f | 31800805 | 29757 | 124910
|  | 348864 | 56003519 | 30492 | 124836
|  | 460896f | 101567881 | 66302 | 250087
|  | 460896 | 183267498 | 66459 | 250129
|  | 6688128f | 239860810 | 109874 | 297573
|  | 6688128 | 392392725 | 111314 | 297375
|  | 6960119f | 198019324 | 118229 | 272055
|  | 6960119 | 344836760 | 116636 | 272042
|  | 8192128f | 256230685 | 124909 | 297915
|  | 8192128 | 412772172 | 125311 | 297838
| Haswell (2013) | 348864f | 37721176 | 34748 | 132554
|  | 348864 | 53245048 | 34256 | 132541
|  | 460896f | 122281345 | 77010 | 266197
|  | 460896 | 173340957 | 75487 | 266055
|  | 6688128f | 288804317 | 128778 | 315443
|  | 6688128 | 461261728 | 128984 | 315809
|  | 6960119f | 247469509 | 135686 | 290003
|  | 6960119 | 334125327 | 138522 | 290075
|  | 8192128f | 316124280 | 146142 | 314719
|  | 8192128 | 483327051 | 146800 | 314483


Microarchitectures are listed in reverse chronological order of when they were introduced.

In the libmceliece distribution,
`command/mceliece-speed.c` measures libmceliece;
`benchmarks/*-*` is the output of `mceliece-speed` on various machines;
and `autogen/md-speed` extracts the table from those measurements.

The table reports only interquartile means of cycle counts.
See the full output files
for differences between multiple measurements and the interquartile mean.
