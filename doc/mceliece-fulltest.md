### NAME

mceliece-fulltest - run the full libmceliece test suite

### SYNOPSIS

    mceliece-fulltest

### DESCRIPTION

`mceliece-fulltest`
runs the full suite of tests of
the libmceliece implementation of the Classic McEliece cryptosystem.

The `mceliece-fulltest` output format is subject to change.
A successful test prints a final line saying

    full tests succeeded

and exits 0.
An unsuccessful test exits nonzero without printing that line.

### SEE ALSO

**mceliece-test**(1),
**mceliece**(1),
**mceliece**(3)
